import router from '@/router';

export default {
  user: {
    authenticated: false,
    name: '',
  },
  login(credentials, redirect) {
    const { username, password } = credentials;
    return new Promise((res, rej) => {
      if (username === 'Admin' && password === '12345') {
        localStorage.setItem('isAuth', 'true');
        localStorage.setItem('userName', username);
        this.user.authenticated = true;
        this.user.name = username;

        if (redirect) {
          router.push(redirect);
        }

        res();
      } else {
        rej('Login error');
      }
    });
  },
  getName() {
    const name = localStorage.getItem('userName');
    this.user.name = name || '';
    return this.user.name;
  },
  checkAuth() {
    const isAuth = localStorage.getItem('isAuth');
    this.user.authenticated = !!isAuth;
  },
};
