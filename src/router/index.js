import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Todo from '../views/Todo';

import Auth from '../core/Auth';

Vue.use(VueRouter);

const routes = [
  {
    path: '/login',
    name: 'Home',
    component: Home,
  },
  {
    path: '/todo',
    name: 'Todo',
    component: Todo,
  },
  {
    path: '*',
    redirect: '/login',
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  Auth.checkAuth();
  if (to.path === '/todo' && !Auth.user.authenticated) {
    next(false);
    router.push('/login');
    return;
  }
  if (to.path === '/login' && Auth.user.authenticated) {
    next(false);
    router.push('/todo');
    return;
  }
  next();
});

export default router;
